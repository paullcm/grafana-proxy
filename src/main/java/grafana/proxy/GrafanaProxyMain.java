package grafana.proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@ComponentScan(basePackages = {"grafana.proxy"})
@EnableZuulProxy
@SpringBootApplication
public class GrafanaProxyMain {

	public static void main(String[] args) {
		SpringApplication.run(GrafanaProxyMain.class, args);
	}

}
